require('dotenv').config();

const express = require('express');
const request = require('then-request');
const bodyparser = require('body-parser');
const md5 = require('md5');
const kv = require("./kamiya_modules/key-value");
const expressJwt = require('express-jwt');
const { Telegraf } = require('telegraf')

//const bot = new Telegraf(process.env.BOT_TOKEN);

const fs = require('fs');

process.env.TZ = 'Asia/Shanghai';

const app = express();

const getToken = function (req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
        return req.query.token;
    } else if (req.headers.cookie) {
        const cookies = req.headers.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.startsWith('session=')) {
                return cookie.substring(8);
            }
        }
    } else if (req.body && req.body.token) {
        return req.body.token;
    }
    return null;
};

app.use(expressJwt.expressjwt({
    secret: process.env.JWT_SECRET,
    algorithms: ['HS256'],
    getToken: getToken
}).unless({
    path: ['/', '/finish_order.html', '/view_product.html', '/index.html', '/favicon.ico', '/api/get_product', '/api/stripe_webhook', '/api/shop/product', '/api/notify', '/redirect.html',
        '/submit.php', '/api/redirect'
    ]
}));

app.use(express.static('public'));

const adminPass = process.env.ADMINPASS;

const D = new kv('./data/data.json');

const S = new kv('./data/pass.json');

const T = new kv('./data/tg.json');

function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
}

/*
app.get('/api/set_login',(req,res) => {
    const { pass } = req.query;
    request('GET',process.env.KAMIYA_URL + `/api/app_gateway/get_token?adminPass=${adminPass}&pass=${pass}`).getBody('utf8').then(JSON.parse).done(
        (R) => {
            if(R.success) {
                S.put(`${decodeURIComponent(pass).token}`,R.token);
                res.send({success: true});
            }
            else res.send({success: false});
        },
        () => res.send({success: false})
    );
});
 */

app.get('/api/get_product',(req,res) => {
    res.send(fs.readFileSync('./data/product.json').toString());
})

app.get('/api/checkJWT',(req,res) => {
    res.send(req.auth);
});

function getProduct(id) {
    const list = JSON.parse(fs.readFileSync('./data/stripe_config.json'));
    for (let i in list) {
        for (let j in list[i].product) {
            if(list[i].product[j].id == id) return list[i].product[j];
        }
    }
    return null;
}

const siteUrl = process.env.SITE_URL;
const paymentUrl = process.env.PAYMENT_URL;
const paymentId = process.env.PAYMENT_ID;
const paymentKey = process.env.PAYMENT_KEY;

const stripe = require('stripe')(process.env.STRIPE_KEY);

app.get('/api/new_stripe_order',async (req,res) => {
    const { id, email } = req.auth;
    let { productId } = req.query;
    const product = getProduct(productId);
    //console.log(product);
    const isInternalEmail = email.includes('@noreply.id.user.pix.ink') || email.includes('@noreply.phone.user.voc.ink')
    if(product) {
        const session = await stripe.checkout.sessions.create({
            line_items: [
                {
                    price: product.price_id,
                    quantity: 1
                }
            ],
            mode: 'payment',
            ... isInternalEmail ? {} : { customer_email: email },
            payment_intent_data: {
                metadata: {
                    origin_email: email,
                }
            },
            success_url: product.reward.type === 'reecho' ? 'https://dash.reecho.ai/billing' : `${siteUrl}/finish_order.html`,
            cancel_url: product.reward.type === 'reecho' ? 'https://dash.reecho.ai/shop' : `${siteUrl}/finish_order.html`,
            phone_number_collection: {
                enabled: false
            },
            tax_id_collection: {
                enabled: false
            }
        });
        //console.log(session);
        const u = session.id;
        //console.log(session)
        console.log(u + ' created ' + productId + ' with email ' + email);
        D.put(`${u}.product_id`,productId);
        D.put(`${u}.pass`,id);
        res.redirect(303, session.url);
    }
    else res.send({success: false});
});

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length === 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        }
    }
    return fmt;
}

const hashid = function () {
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    const length = 6;
    let result = '';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

const messageSendWaitPromises = {};

async function processLog(h, type, attach) {
    return;
    /*
    if(messageSendWaitPromises[h]) {
        try {
            await messageSendWaitPromises[h];
        }
        catch (e) {
            console.log(e);
        }
    }
    const messageId = T.get(h + '.messageId');
    let before = T.get(h + '.before');
    try {
        if(messageId) {
            before += `\n${dateFormat('HH:MM:SS', new Date())}: ${attach}`;
            messageSendWaitPromises[h] = bot.telegram.editMessageText(process.env.CHAT_ID, messageId, null, before).then((r) => {
                delete messageSendWaitPromises[h];
                return r;
            });
            const r = await messageSendWaitPromises[h];
            T.put(h + '.before', before);
        }
        else {
            before = `[${dateFormat('YYYY-mm-dd HH:MM:SS', new Date())}] [${type}] [${h}]\n${dateFormat('HH:MM:SS', new Date())}: ${attach}`;
            messageSendWaitPromises[h] = bot.telegram.sendMessage(process.env.CHAT_ID, before).then((r) => {
                delete messageSendWaitPromises[h];
                return r;
            });
            const r = await messageSendWaitPromises[h];
            T.put(h + '.messageId', r.message_id);
            T.put(h + '.before', before);
        }
    }
    catch (e) {
        console.log(e);
    }
     */
}

app.post('/api/stripe_webhook',bodyparser.raw({type: 'application/json'}),async (req,res) => {
    //console.log(req.body);
    const payload = req.body;

    let event;

    try {
        const sig = req.headers['stripe-signature'];
        //console.log(sig);
        event = stripe.webhooks.constructEvent(payload,sig,process.env.STRIPE_ENDPOINT_KEY);
    } catch (err) {
        console.log(err)
        return res.status(400).send(`Webhook Error`);
    }
    const hid = hashid();
    if(event.type == 'checkout.session.completed') {
        //console.log(event);
        const session = event.data.object;
        //console.log(session);
        const u = session.id;
        //console.log(session);
        const pass = D.get(`${u}.pass`);
        processLog(hid, 'stripe webhook', 'received ' + event.type + ' for ' + u);
        if(typeof pass === 'string' && pass.match(/isSubmit$/)) {
            const rU = pass.substring(0,pass.length - 9);
            const meta = JSON.parse(D.get(`${rU}.meta`));
            processLog(hid, 'stripe webhook', `charged ${meta.money} for ${meta.name}`);
            //process notify
            const notifyBodyNoencode = `money=${meta.money}&name=${meta.name}&out_trade_no=${meta.out_trade_no}&pid=${meta.holder.id}&trade_no=${session.payment_intent || session.id}&trade_status=TRADE_SUCCESS`;
            let notifyBody = `money=${encodeURIComponent(meta.money)}&name=${encodeURIComponent(meta.name)}&out_trade_no=${encodeURIComponent(meta.out_trade_no)}&pid=${encodeURIComponent(meta.holder.id)}&trade_no=${encodeURIComponent(session.payment_intent || session.id)}&trade_status=TRADE_SUCCESS`;
            const md5Sign = md5(notifyBodyNoencode + meta.holder.key);
            notifyBody += `&sign=${md5Sign}&sign_type=MD5`;
            console.log(meta.notify_url, notifyBody);
            res.send({
                success: true,
                message: 'Webhook received! notify started'
            });
            const notifyRes = await request('GET',meta.notify_url + '?' + notifyBody);
            if(notifyRes.statusCode != 200) {
                console.log('notify failed');
                processLog(hid, 'stripe webhook', `notify status code not 200, start retry`);
                let notifyRes2 = await request('GET',meta.notify_url + '?' + notifyBody);
                let count = 0;
                while(notifyRes2.statusCode != 200 && count < 3) {
                    console.log('retrying notify ' + count);
                    notifyRes2 = await request('GET',meta.notify_url + '?' + notifyBody);
                    count++;
                }
                if(notifyRes2.statusCode != 200) {
                    console.log('retry notify failed');
                    processLog(hid, 'stripe webhook', `notify failed after 3 retry`);
                }
                else {
                    console.log('retry notify success');
                    processLog(hid, 'stripe webhook', `notify success after ${count} retry`);
                }
                return;
            }
            const resBody = notifyRes.getBody('utf8');
            console.log(resBody);
            if(resBody != 'success') {
                console.log('notify failed');
                processLog(hid, 'stripe webhook', `notify rejected`);
                return;
            }
            processLog(hid, 'stripe webhook', `notify success`);
            return;
        }
        const productId = D.get(`${u}.product_id`);
        console.log(pass,productId);
        if(pass && productId) {
            const product = getProduct(productId);
            //console.log(product);
            if(product) {
                console.log(product);
                if(S.get(`${u}.processed`)) {
                    res.send({
                        success: true,
                        message: 'Webhook received! a reward already processed'
                    });
                    return;
                }
                if(product.reward.type == 'left') {
                        request('POST',process.env.KAMIYA_URL + `/api/billing/addCredit`,{
                            json: {
                                adminPass: adminPass,
                                id: pass,
                                credit: product.reward.count
                            }
                        }).getBody('utf8').then(JSON.parse).done(
                            (R) => {
                                console.log('processed a reward for left',product.reward.count);
                                S.put(`${u}.processed`,true);
                                res.send({
                                    success: true,
                                    message: 'Webhook received! a reward proceseed',
                                    data: R
                                });
                            },
                            (e) => {
                                console.log(e);
                                res.send({
                                    success: true,
                                    message: 'Webhook received! a reward procese failed'
                                });
                            }
                        );
                }
                else if(product.reward.type == 'access') {
                        request('GET',process.env.KAMIYA_URL + `/api/app_gateway/set_access?adminPass=${adminPass}&pass=${encodeURIComponent(pass)}&access=${product.reward.access}`).getBody('utf8').then(JSON.parse).done(
                            (R) => {
                                console.log('processed a reward for access',product.reward.access);
                                S.put(`${u}.processed`,true);
                                res.send({
                                    success: true,
                                    message: 'Webhook received! a reward proceseed'
                                });
                            },
                            (e) => {
                                console.log(e);
                                res.send({
                                    success: true,
                                    message: 'Webhook received! a reward procese failed'
                                });
                            }
                        );
                }
                else if(product.reward.type == 'reecho') {
                    request('POST',process.env.REECHO_URL + `/api/billing/doneBuyBySystem`,{
                        json: {
                            backendToken: process.env.REECHO_TOKEN,
                            credit: product.reward.credit,
                            id: pass
                        }
                    }).getBody('utf8').then(JSON.parse).done(
                        (R) => {
                            console.log('processed a reward for reecho',product.reward.credit);
                            S.put(`${u}.processed`,true);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward proceseed',
                                data: R
                            });
                        },
                        (e) => {
                            console.log(e);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward procese failed'
                            });
                        }
                    );
                }
                else if(product.reward.type == 'reecho-lora') {
                    request('POST',process.env.REECHO_URL + `/api/billing/doneBuyBySystem`,{
                        json: {
                            backendToken: process.env.REECHO_TOKEN,
                            loraQuota: product.reward.quota,
                            id: pass
                        }
                    }).getBody('utf8').then(JSON.parse).done(
                        (R) => {
                            console.log('processed a reward for reecho',product.reward.credit);
                            S.put(`${u}.processed`,true);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward proceseed',
                                data: R
                            });
                        },
                        (e) => {
                            console.log(e);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward procese failed'
                            });
                        }
                    );
                }
                else if(product.reward.type == 'package') {
                    request('POST',process.env.KAMIYA_URL + `/api/colorMC/subcription/addByBilling`,{
                        json: {
                            adminPass: adminPass,
                            id: pass,
                            userPackage: product.reward.package,
                            duration: product.reward.duration
                        }
                    }).getBody('utf8').then(JSON.parse).done(
                        (R) => {
                            console.log('processed a reward for package', product.reward.duration, product.reward.package);
                            S.put(`${u}.processed`,true);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward proceseed',
                                data: R
                            });
                        },
                        (e) => {
                            console.log(e);
                            res.send({
                                success: true,
                                message: 'Webhook received! a reward procese failed'
                            });
                        }
                    );
                }
                else {
                    res.send({
                        success: true,
                        message: 'Webhook received! reward is custom'
                    });
                }
            } else res.send({
                success: true,
                message: 'Webhook received! no product found'
            });
        } else res.send({
            success: true,
            message: 'Webhook received! no payment found'
        });
    }
    else res.send({
        success: true,
        message: 'Webhook received! no reward processed'
    });
});

app.get('/api/shop/product',(req,res) => {
    res.send(getProduct(req.query.id));
});

app.get('/api/payment/new_order',(req,res) => {
    const { id, email } = req.auth;
    let { pass, productId } = req.query;
    let method = req.query.method || 'alipay';
    const product = getProduct(productId);
    //console.log(product,productId);
    if(productId && product) {
        const u = uuid();
        D.put(`${u}.product_id`,productId);
        D.put(`${u}.pass`,id);
        D.put(`${u}.meta`,JSON.stringify({
            email
        }));

        const sign_text = `money=${product.price}&name=${product.reward.type.includes('reecho') ? 'Reecho' : 'Kamiya'}_${productId}_${u}&notify_url=${siteUrl}/api/notify&out_trade_no=${u}&pid=${paymentId}&return_url=${product.reward.type.includes('reecho') ? 'https://dash.reecho.ai/billing' : `${siteUrl}/finish_order.html`}&sitename=${product.reward.type.includes('reecho') ? 'Reecho.AI' : 'KamiyaShop'}&type=${method}${paymentKey}`

        res.setHeader('Content-Type','text/html; charset=utf-8');
        res.send(`
            <html>
                <body>
                    <script>
                        function Post(URL, PARAMTERS) {
                            var temp_form = document.createElement("form");
                            temp_form.action = URL;
                            temp_form.target = "_self";
                            temp_form.method = "post";
                            temp_form.style.display = "none";
                            for (var item in PARAMTERS) {
                                var opt = document.createElement("textarea");
                                opt.name = PARAMTERS[item].name;
                                opt.value = PARAMTERS[item].value;
                                temp_form.appendChild(opt);
                            }
                            document.body.appendChild(temp_form);
                            temp_form.submit();
                        };
                        Post('${paymentUrl}/submit.php',[
                            {name:'pid',value:'${paymentId}'},
                            {name:'type',value:'${method}'},
                            {name:'out_trade_no',value:'${u}'},
                            {name:'notify_url',value:'${siteUrl}/api/notify'},
                            {name:'return_url',value:'${product.reward.type.includes('reecho') ? 'https://dash.reecho.ai/billing' : `${siteUrl}/finish_order.html`}'},
                            {name:'name',value:'${product.reward.type.includes('reecho') ? 'Reecho' : 'Kamiya'}_${productId}_${u}'},
                            {name:'money',value:'${product.price}'},
                            {name:'sitename',value:'${product.reward.type.includes('reecho') ? 'Reecho.AI' : 'KamiyaShop'}'},
                            {name:'sign',value:'${md5(sign_text)}'},
                            {name:'sign_type',value:'MD5'}
                            ]
                        );
                    </script>
                </body>
            </html>
        `);
    }
    else res.send({success: false,msg: '请先登录'});
});

app.get('/api/notify',(req,res) => {
    if(!req.query.out_trade_no || !req.query.sign) {
        console.log('New Failed Callback');
        res.send('Access is forbidden, illegal parameter.');
        return;
    };
    const q = req.query;
    const sign_text = `money=${q.money}&name=${q.name}&out_trade_no=${q.out_trade_no}&pid=${paymentId}&trade_no=${q.trade_no}&trade_status=TRADE_SUCCESS&type=${q.type}${paymentKey}`;
    if(md5(sign_text) != q.sign) {
        console.log('New Failed Callback');
        res.send('Access is forbidden, illegal parameter.');
        return;
    };
    const u = q.out_trade_no;
    if(S.get(`${u}.success`) && S.get(`${u}.processed`)) {
        res.send('Access is forbidden, illegal parameter.');
        return;
    }
    S.put(`${u}.success`,true);
    const productId = D.get(`${u}.product_id`);
    const pass = D.get(`${u}.pass`);
    const product = getProduct(productId);
    switch (product.reward.type) {
        case 'left': {
            request('POST',process.env.KAMIYA_URL + `/api/billing/addCredit`,{
                json: {
                    adminPass: adminPass,
                    id: pass,
                    credit: product.reward.count
                }
            }).getBody('utf8').then(JSON.parse).done(
                (R) => {
                    console.log('processed a reward for left',product.reward.count);
                    S.put(`${u}.processed`,true);
                },
                (e) => {
                    console.log(e);
                }
            );
            break;
        }
        case 'access': {
            request('GET',process.env.KAMIYA_URL + `/api/app_gateway/set_access?adminPass=${adminPass}&pass=${encodeURIComponent(pass)}&access=${product.reward.access}`).getBody('utf8').then(JSON.parse).done(
                (R) => {
                    console.log('processed a reward for access',product.reward.access);
                    S.put(`${u}.processed`,true);
                },
                (e) => console.log(e)
            );
            break;
        }
        case 'reecho': {
            request('POST',process.env.REECHO_URL + `/api/billing/doneBuyBySystem`,{
                json: {
                    backendToken: process.env.REECHO_TOKEN,
                    credit: product.reward.credit,
                    id: pass
                }
            }).getBody('utf8').then(JSON.parse).done(
                (R) => {
                    console.log('processed a reward for reecho',product.reward.credit);
                    S.put(`${u}.processed`,true);
                },
                (e) => console.log(e)
            );
            break;
        }
        case 'reecho-lora': {
            request('POST',process.env.REECHO_URL + `/api/billing/doneBuyBySystem`,{
                json: {
                    backendToken: process.env.REECHO_TOKEN,
                    loraQuota: product.reward.quota,
                    id: pass
                }
            }).getBody('utf8').then(JSON.parse).done(
                (R) => {
                    console.log('processed a reward for reecho',product.reward.credit);
                    S.put(`${u}.processed`,true);
                },
                (e) => console.log(e)
            );
            break;
        }
        default: {
            break;
        }
    }
    res.send({success: true});
});

app.get('/api/redirect',(req,res) => {
    const rU = req.query.u;
    const meta = JSON.parse(D.get(`${rU}.meta`));
    let return_url = meta.return_url;
    if(req.query.cancel) return_url = meta.holder.cancelUrl;
    const redirected = S.get(`${rU}.redirected`);
    if(redirected) {
        res.redirect(303, `${siteUrl}/finish_order.html`);
    }
    else {
        res.redirect(303, return_url);
        S.put(`${rU}.redirected`,true);
    }
});

function getHolder(id) {
    const list = JSON.parse(fs.readFileSync('./data/holder.json'));
    for (let i in list) {
        if(list[i].id == id) return list[i];
    }
    return null;
}

app.use(express.urlencoded({extended: true}));

app.post('/submit.php', async (req,res) => {
    const q = req.body;
    const { pid } = q;
    const holder = getHolder(pid);
    if(!holder) {
        res.send('Access is forbidden, illegal parameter.');
        return;
    }
    const keys = Object.keys(q).sort();
    let sign_text = '';
    for(let i in keys) {
        if(keys[i] == 'sign' || keys[i] == 'sign_type' || q[keys[i]] == '') continue;
        sign_text += `${keys[i]}=${q[keys[i]]}&`;
    }
    sign_text = sign_text.substring(0,sign_text.length - 1);
    console.log(sign_text + holder.key, md5(sign_text + holder.key));
    if(md5(sign_text + holder.key) != q.sign) {
        res.send('Access is forbidden, illegal parameter.');
        return;
    }
    const hid = hashid();
    processLog(hid, 'checkout session', `new checkout session for ${q.name} by ${req.headers['x-forwarded-for']}`);
    if(parseInt(q.money * 110) < 440) {
        processLog(hid, 'checkout session', `checkout session for HK$${parseInt(q.money * 110)} failed, money too low`);
        res.send('最低支付金额为4元，低于此金额请考虑进行余额充值后再支付账单');
        return;
    }
    const u = q.out_trade_no;
    const rU = holder.name + '-' + u;
    q.holder = holder;
    D.put(`${rU}.meta`, JSON.stringify(q));
    const session = await stripe.checkout.sessions.create({
        line_items: [
            {
                price_data: {
                    currency: 'hkd',
                    product_data: {
                        name: 'Crystal (by Qty) - #' + q.out_trade_no
                    },
                    unit_amount: parseInt(q.money * 110)
                },
                quantity: 1
            }
        ],
        mode: 'payment',
        success_url: `${siteUrl}/api/redirect?u=${rU}`,
        cancel_url: `${siteUrl}/api/redirect?u=${rU}&cancel=true`,
        automatic_tax: {
            enabled: false
        },
        phone_number_collection: {
            enabled: false
        },
        tax_id_collection: {
            enabled: false
        }
    });
    const sid = session.id;
    processLog(hid, 'checkout session', `checkout session created with id ${sid}, wait for HK$${parseInt(q.money * 110) / 100} payment`);
    D.put(`${sid}.pass`,rU + '.isSubmit');
    res.redirect(303, session.url);
});

app.listen(process.env.PORT || 11683, () => {
    console.log('Server started at ' + (process.env.PORT || 11683) + '.');
});

